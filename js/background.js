var loaded = false;
var re = new RegExp(/(https:\/\/|http:\/\/)(eksisozluk.com)\/\S+--\d+\S+/);
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    let url = tab.url;
    if (changeInfo.status == 'complete' && tab.status == 'complete' && tab.url != undefined) {
        if (re.test(tab.url)) {
            loaded = !loaded;

            chrome.storage.sync.get(['eksitools_deleted'], function(result) {
                var deleted_status = result.eksitools_deleted;
                if (deleted_status == undefined || deleted_status == null || deleted_status == "") {
                    chrome.storage.sync.set({"eksitools_deleted": "true"}, function() {});
                }
            });

            chrome.scripting.executeScript({files: ["vendors/jquery.js"], target: { tabId: tabId }}, function(result) {
                chrome.scripting.executeScript({files: ["vendors/bootstrap.js"], target: { tabId: tabId }}, function(result) {
                    chrome.scripting.executeScript({files: ["vendors/diff.js"], target: { tabId: tabId }}, function(result) {
                        chrome.scripting.insertCSS({files: ["css/main.css"], target: { tabId: tabId }}, function(result) {
                            chrome.scripting.executeScript({files: ["js/main.js"], target: { tabId: tabId }}, function(result) {
                            });
                        });
                    });
                });
            });
        }   
    }
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.message == "getDeletedStatus") {
        chrome.storage.sync.get(['eksitools_deleted'], function(result) {
            var status = result.eksitools_deleted;
            sendResponse({status: status });
        });
    }
    else
      sendResponse({});
    
    return true;
});