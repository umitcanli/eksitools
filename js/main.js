var apiBaseUrl, eksiApi, keys;

$.getJSON(chrome.runtime.getURL('config.json'), function(json) {
    apiBaseUrl = json["api-urls"].eksitools;
    eksiApi = json["api-urls"].eksi;
    keys = json.keys;
});

$(".sub-title-menu").append(`<div><div class="circle hidden"></div><a href="javascript:;" id="live-btn">canlı</a></div>`);

$(".feedback").prepend(`
<a href="javascript:;" class="archive-button" style="margin-right: 24px;" title="arşivle">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#bdbdbd"/>
        <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#bdbdbd" opacity="0.8"/>
    </g>
</svg>
</a><a href="javascript:;" class="archives-button" title="arşivi görüntüle">arşiv</a>`);

var url = window.location.href;
var topicId = url.split("--")[1].split("?")[0];

chrome.runtime.sendMessage({ message: "getDeletedStatus"}, function(message) {
    var eksitools_deleted = message.status;

    if (url.match(/.*\?\a/g) != undefined || url.match(/.*\?(day)/g) != undefined || url.match(/.*\?\p/g) != undefined || url.match(/(https:\/\/|http:\/\/)(eksisozluk.com)\/\S+--\d+/g) != undefined) {
        var cpage = parseInt($(".pager").attr("data-currentpage"));
        var cpage_count = parseInt($(".pager").attr("data-pagecount"));
        
        var entries = [];
        $.each($("#entry-item-list").children("li"), function (index, item) {
            entries.push(parseInt($(item).attr("data-id")));
        });
    
        var data = { TopicId: parseInt(topicId), Page: cpage, PageCount: cpage_count, Entries: entries };
        $.ajax({
            url: apiBaseUrl + "archive/get-archives",
            type: "POST",
            data: JSON.stringify(data),
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            success: function (response) {
                if (eksitools_deleted == "true") {
                    for (let i = 0; i < response.deleted.length; i++) {
                        const entry = response.deleted[i]; 
                        
                        var str = `<li data-id="${entry.entryId}"
                                    data-flags="share msg report vote entrymodlog favorite"
                                    data-ispinned="false"
                                    class="hidden">
                                        <div class="content content-expanded">${markup(entry.content)}</div>
                                        <footer>
                                            <div class="feedback">
                                            <a href="javascript:;" class="archives-button" title="arşivi görüntüle">arşiv</a>
                                                <span class="favorite-links" style="">
                                                    <a class="favorite-link">
                                                        <svg class="eksico">
                                                            <use xlink:href="#eksico-drop"></use>
                                                        </svg>
                                                    </a>
                                                    <a class="favorite-count toggles">${entry.favoriteCount == 0 ? "" : entry.favoriteCount}</a>
                                                    <div class="favorite-list-popup toggles-menu">
                                                        <div></div>
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="info">
                                                <a class="entry-date permalink" href="/entry/${entry.entryId}">${getEntryDate(entry.entryCreatedDate, entry.entryUpdatedDate)}</a>
                                                <a class="entry-author" href="/biri/${entry.author.replace(" ", "-")}">${entry.author}</a>
                                            </div>
                                        </footer>
                                    </li>`;
                        
                        var firstEntry = entries.find(p => p > entry.entryId);
                        $(`#entry-item-list li[data-id=${firstEntry}]`).before(str);
                    }
                }
    
                for (let i = 0; i < response.counts.length; i++) {
                    const element = response.counts[i];
                    
                    $(`#entry-item-list li[data-id=${element.entryId}] .feedback .archives-button`).append(` (${element.count})`);
                }
            },
            error: function () {
                console.log("err");
            }
        });
    }
});

var liveon = false;
var updateInterval;
var token = "";
var page = 1;
var first = true;
var lastEntry = 1;
$("#live-btn").on("click", function () {
    if (liveon) {
        return;
    }
    liveon = true;

    //css
    $(".nice-mode-toggler a").removeClass("nice-on");
    $("#entry-item-list").html("");
    $("#topic .pager").remove();
    $(".showall ").remove();
    $("#live-btn").addClass("clicked");
    $(".circle").removeClass("hidden");

    //update
    checkToken();
    updateInterval = setInterval(function () {
        checkToken();
    }, 5000);
});

function checkToken() {
    if (token == "") {
        chrome.storage.local.get(["eksi_access_token"], function(result) {
            if (result.eksi_access_token == undefined || result.eksi_access_token == null || result.eksi_access_token == "") {
                getAccessToken();
            }
            else {
                var tokenObj = JSON.parse(result.eksi_access_token);
                if (new Date().getTime() > new Date(tokenObj.createdAt).getTime() + (86400 * 1000)) {
                    getAccessToken();
                }
                else {
                    token = tokenObj.token;
                    updateEntries();
                }
            }
        });
    }
    else {
        updateEntries();
    }
}

function updateEntries() {
    $.ajax({
        url: `${eksiApi}topic/${topicId}?p=${page}` ,
        type: "GET",
        beforeSend: function(request) {
            request.setRequestHeader("Client-Secret", "eabb8841-258d-4561-89a6-66c6501dee83");
            request.setRequestHeader("Authorization", "Bearer " + token);
        },
        success: function (response) {
            if (first) {
                page = response.Data.PageCount;
                first = false;
                updateEntries();
            }
            else {
                var pageCount = response.Data.PageCount;
                if (page <= pageCount) {
                    page = pageCount;
                    
                    for (let i = 0; i < response.Data.Entries.length; i++) {
                        const entry = response.Data.Entries[i];
                        if (entry.Id > lastEntry) {
                            var content = markup(entry.Content);

                            var str = `<li data-id="${entry.Id}" 
                            data-author="${entry.Author.Nick}" 
                            data-author-id="${entry.Author.Id}" 
                            data-flags="share msg report vote entrymodlog favorite" 
                            data-isfavorite="${entry.IsFavorite}" 
                            data-ispinned="false" 
                            data-favorite-count="${entry.FavoriteCount}" 
                            data-seyler-slug="" 
                            data-comment-count="${entry.CommentCount}"
                            class="new-entry">
                                <div class="content content-expanded">${content}</div>
                                <footer>
                                    <div class="feedback">
                                        <a href="javascript:;" class="archive-button" style="margin-right: 24px;" title="arşivle">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18px" height="18px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M10.9630156,7.5 L11.0475062,7.5 C11.3043819,7.5 11.5194647,7.69464724 11.5450248,7.95024814 L12,12.5 L15.2480695,14.3560397 C15.403857,14.4450611 15.5,14.6107328 15.5,14.7901613 L15.5,15 C15.5,15.2109164 15.3290185,15.3818979 15.1181021,15.3818979 C15.0841582,15.3818979 15.0503659,15.3773725 15.0176181,15.3684413 L10.3986612,14.1087258 C10.1672824,14.0456225 10.0132986,13.8271186 10.0316926,13.5879956 L10.4644883,7.96165175 C10.4845267,7.70115317 10.7017474,7.5 10.9630156,7.5 Z" fill="#bdbdbd"/>
                                                    <path d="M7.38979581,2.8349582 C8.65216735,2.29743306 10.0413491,2 11.5,2 C17.2989899,2 22,6.70101013 22,12.5 C22,18.2989899 17.2989899,23 11.5,23 C5.70101013,23 1,18.2989899 1,12.5 C1,11.5151324 1.13559454,10.5619345 1.38913364,9.65805651 L3.31481075,10.1982117 C3.10672013,10.940064 3,11.7119264 3,12.5 C3,17.1944204 6.80557963,21 11.5,21 C16.1944204,21 20,17.1944204 20,12.5 C20,7.80557963 16.1944204,4 11.5,4 C10.54876,4 9.62236069,4.15592757 8.74872191,4.45446326 L9.93948308,5.87355717 C10.0088058,5.95617272 10.0495583,6.05898805 10.05566,6.16666224 C10.0712834,6.4423623 9.86044965,6.67852665 9.5847496,6.69415008 L4.71777931,6.96995273 C4.66931162,6.97269931 4.62070229,6.96837279 4.57348157,6.95710938 C4.30487471,6.89303938 4.13906482,6.62335149 4.20313482,6.35474463 L5.33163823,1.62361064 C5.35654118,1.51920756 5.41437908,1.4255891 5.49660017,1.35659741 C5.7081375,1.17909652 6.0235153,1.2066885 6.2010162,1.41822583 L7.38979581,2.8349582 Z" fill="#bdbdbd" opacity="0.8"/>
                                                </g>
                                            </svg>
                                        </a>
                                        <a href="javascript:;" class="archives-button" title="arşivi görüntüle">arşiv</a>
                                        <span class="share-links">
                                            <a target="_blank" rel="nofollow noopener" href="#" aria-label="facebook" title="facebook" class="svgico svgico-facebook">
                                                <svg class="eksico" id="svg-icon-facebook">
                                                    <use xlink:href="#eksico-icon-facebook"></use>
                                                </svg>
                                            </a>
                                            <a target="_blank" rel="nofollow noopener" href="/entry/tweet/${entry.Id}" aria-label="twitter" title="twitter" class="svgico svgico-twitter">
                                                <svg class="eksico" id="svg-icon-twitter">
                                                    <use xlink:href="#eksico-icon-twitter"></use>
                                                </svg>
                                            </a>
                                        </span>
                                        <span class="rate-options">
                                            <a class="like" title="şükela!">
                                                <span></span>
                                                <svg class="eksico" id="svg-chevron-up">
                                                    <use xlink:href="#eksico-chevron-up-thick"></use>
                                                </svg>
                                            </a>
                                            <a class="dislike" title="çok kötü">
                                                <span></span>
                                                <svg class="eksico" id="svg-chevron-down">
                                                    <use xlink:href="#eksico-chevron-down-thick"></use>
                                                </svg>
                                            </a>
                                        </span>
                                        <span class="favorite-links" style="">
                                            <a class="favorite-link" title="favorilere ekle" aria-label="favorilere ekle">
                                                <svg class="eksico">
                                                    <use xlink:href="#eksico-drop"></use>
                                                </svg>
                                            </a>
                                            <a class="favorite-count toggles">${entry.FavoriteCount == 0 ? "" : entry.FavoriteCount}</a>
                                            <div class="favorite-list-popup toggles-menu">
                                                <div></div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="info">
                                        <a class="entry-date permalink" href="/entry/${entry.Id}">${getEntryDate(entry.Created, entry.LastUpdated)}</a>
                                        <a class="entry-author" href="/biri/${entry.Author.Nick.replace(" ", "-")}">${entry.Author.Nick}</a>
                                        <div class="other dropdown">
                                            <a class="dropdown-toggle toggles" title="diğer">
                                                <svg class="eksico" id="svg-dots">
                                                    <use xlink:href="#eksico-dots"></use>
                                                </svg>
                                            </a>
                                            <ul class="dropdown-menu right toggles-menu">
                                                <li>
                                                    <a title="mesaj gönder" aria-label="mesaj gönder">mesaj gönder</a>
                                                </li>
                                                <li>
                                                    <a class="report-link" title="aynen öyle" href="/iletisim?RefEntryId=${entry.Id}&amp;Category=Content&amp;">şikayet</a>
                                                </li>
                                                <li>
                                                    <a href="/modlog?q=%23${entry.Id}">modlog</a>
                                                </li>
                                                <li>
                                                    <a href="#">engelle</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </footer>
                            </li>`;
                            $("#entry-item-list").prepend(str);
                        }
                    }

                    lastEntry = response.Data.Entries.at(-1).Id;
                }
            }
        },
        error: function () {
            console.log("err");
        }
    });
}

function getEntryDate (created, updated) {
    var createdDate = new Date(created);
    var createdTime = createdDate.toTimeString().split(" ")[0].split(":");
    var str = ((createdDate.getDate() > 9) ? createdDate.getDate() : ('0' + createdDate.getDate())) + "." + ((createdDate.getMonth() > 8) ? (createdDate.getMonth() + 1) : ('0' + (createdDate.getMonth() + 1))) + "." + createdDate.getFullYear() + " " + createdTime[0] + ":" + createdTime[1];
    if (updated != null && updated != undefined && updated != "") {
        var updatedDate = new Date(updated);
        var updatedTime = updatedDate.toTimeString().split(" ")[0].split(":");

        str += " ~ ";
        if (createdDate.getDate() == updatedDate.getDate() && createdDate.getMonth() == updatedDate.getMonth() && createdDate.getFullYear() == updatedDate.getFullYear()) {
            str += updatedTime[0] + ":" + updatedTime[1];
        }
        else {
            str += ((updatedDate.getDate() > 9) ? updatedDate.getDate() : ('0' + updatedDate.getDate())) + "." + ((updatedDate.getMonth() > 8) ? (updatedDate.getMonth() + 1) : ('0' + (updatedDate.getMonth() + 1))) + "." + updatedDate.getFullYear() + " " + updatedTime[0] + ":" + updatedTime[1];

        }
    }

    return str;
}

function markup(entryContent) {
    entryContent = entryContent.replace(new RegExp(/(?<!\[)(http:\/\/|https:\/\/)\S+/g), function (match, offset, string) {
        match = decodeURIComponent(match.trim());
        var pos = getPosition(match, "/", 3);
        var url = match.substring(0, pos);
        var txt = url + "/..." + match.substring(match.length - (50 - url.length));
        return `<a rel="nofollow noopener" class="url" target="_blank" href="${match}" title="${match}">${txt}</a>`; 
    });

    entryContent = entryContent.replace(new RegExp(/(\r\n|\n|\r)/gm), "<br />");

    entryContent = entryContent.replace(new RegExp(/\((bkz:[^)]+)\)/g), function (match, offset, string) {
        var txt = match.replace("(bkz:", "").replace(")", "").replace(" ", "");
        if (txt.startsWith("#")) {
            return `(bkz: <a class="b" href="/entry/${txt.replace("#", "")}">${txt}</a>)`; 
        }
        else {
            return `(bkz: <a class="b" href="/?q=${txt.replaceAll(" ", "+")}">${txt}</a>)`; 
        }
    });

    entryContent = entryContent.replace(new RegExp(/(\`[\w\s]+\`)/g), function (match, offset, string) {
        if (match.length > 52) {
            return match;
        }

        var txt = match.replaceAll("`", "");
        return `<a class="b" href="/?q=${txt.replaceAll(" ", "+")}">${txt}</a>`;
    });

    entryContent = entryContent.replace(new RegExp(/`:.+`/g), function (match, offset, string) {
        if (match.length > 53) {
            return match;
        }

        var txt = match.replaceAll("`", "").replace(":", "");
        return `<sup class="ab"><a title="(bkz: ${txt})" class="b" href="/?q=${txt.replaceAll(" ", "+")}" data-query="${txt}">*</a></sup>`;
    });

    entryContent = entryContent.replace(new RegExp(/\((ara:[^)]+)\)/g), function (match, offset, string) {
        var txt = match.replace("(ara:", "").replace(")", "").replace(" ", "");
        return `(ara: <a class="ara index-link" href="/basliklar/ara?searchform.keywords=${txt}">${txt}</a>)`; 
    });

    entryContent = entryContent.replace(new RegExp(/\[(https:\/\/|http:\/\/).+\]/g), function (match, offset, string) {
        var link = match.split(" ")[0].replace("[", "");
        var title = match.replace(link, "").replace("[", "").replace("]", "");                               
        return `<a rel="nofollow noopener" class="url" target="_blank" href="${link}" title="${title}">${title}</a>`; 
    });

    return entryContent;
}

function getPosition(str, pat, n){
    var L= str.length, i= -1;
    while(n-- && i++<L){
        i= str.indexOf(pat, i);
        if (i < 0) break;
    }
    return i;
}

function getAccessToken() {
    var data = {
        "Platform": "g",
        "Version": "2.0.0",
        "Build": 51,
        "Api-Secret": "68f779c5-4d39-411a-bd12-cbcc50dc83dd",
        "Client-Secret": "eabb8841-258d-4561-89a6-66c6501dee83",
        "ClientUniqueId": "1a62383d-742e-4bcf-bf77-2fe1a1edcd39"
    }

    $.ajax({
        url: eksiApi + "account/anonymoustoken",
        type: "POST",
        data: data,
        success: function (response) {
            var tokenObj = { token: response.Data.access_token, createdAt: new Date().getTime() }
            chrome.storage.local.set({"eksi_access_token": JSON.stringify(tokenObj)}, function() {
                console.log(response.Data.access_token);
                checkToken();
            });
        },
        error: function () {
            console.log("Error while retrieving access token!");
            $("#entry-item-list").html(`<li style="text-align: center;">Bir hata oluştu! Sayfayı yenileyerek tekrar deneyiniz...</li>`);
            clearInterval(updateInterval);
        }
    });
}

$(document).on("click", ".archive-button", function () {
    var entryId = $(this).closest("li").data("id");
    $(".archive-button").attr("disabled");
    $.ajax({
        url: apiBaseUrl +"archive/add/" + entryId,
        type: "GET",
        success: function (response) {
            if (response.status) {
                alert(response.message);
                $(".archive-button").removeAttr("disabled");
            }
            else {
                alert(response.message);
                $(".archive-button").removeAttr("disabled");
            }
        },
        error: function () {
            alert("Sunucuya bağlanırken bir hata oluştu!");
            $(".archive-button").removeAttr("disabled");
        }
    });
});

$(document).on("click", ".archives-button", function () {
    var id = $(this).closest("li").data("id");

    $.ajax({
        url: apiBaseUrl + "archive/get/" + id,
        type: "GET",
        success: function (response) {
            if (response.length == 0) {
                alert("Bu entrye ait arşiv bulunamadı!");
            }
            else {
                var str = "";
                for (let i = 1; i <= response.length; i++) {
                    const element = response[i - 1];

                    var createdDate = new Date(element.createdDate);
                    var createdTime = createdDate.toTimeString().split(" ")[0].split(":");

                    var entryDate = new Date(element.entryCreatedDate);
                    var entryCreatedTime = entryDate.toTimeString().split(" ")[0].split(":");

                    var entryDateStr = `${entryDate.toJSON().slice(0,10).split('-').reverse().join('.')} ${entryCreatedTime[0]}:${entryCreatedTime[1]}`;
                    if (element.entryUpdatedDate != null) {
                        var entryUpdatedDate = new Date(element.entryUpdatedDate);
                        var entryUpdatedTime = entryUpdatedDate.toTimeString().split(" ")[0].split(":");

                        if (entryDate.getDate() == entryUpdatedDate.getDate() && entryDate.getMonth() == entryUpdatedDate.getMonth() && entryDate.getFullYear() == entryUpdatedDate.getFullYear()) {
                            entryDateStr += ` ~ ${entryUpdatedTime[0]}:${entryUpdatedTime[1]}`;
                        }
                        else {
                            entryDateStr += ` ~ ${createdDate.toJSON().slice(0,10).split('-').reverse().join('.')} ${entryUpdatedTime[0]}:${entryUpdatedTime[1]}`;
                        }
                    }
                    str += `
                        <div class="card">
                            <div class="card-header" id="heading${i}">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-target="#collapse${i}" aria-expanded="${ i == 1 ? "true" : "false" }" aria-controls="collapse${i}">
                                        <div class="card-title">
                                            <strong>${createdDate.toJSON().slice(0,10).split('-').reverse().join('.')} ${createdTime[0] + ":" + createdTime[1]}</strong><small>tarihli arşiv</small>
                                        </div>
                                    </a>
                                </h5>
                                ${i == response.length ? "" : "<div class='compare'><a class='compare-btn'>öncekiyle karşılaştır</a></div>"}
                            </div>
                            <div id="collapse${i}" class="collapse${ i == 1 ? " show" : ""}" aria-labelledby="heading${1}" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="archive-wrapper">
                                        ${$("#title").prop('outerHTML')}
                                        <div class="archive-content">
                                            ${markup(element.content)}
                                        </div>
                                    </div>
                                    <div class="archive-feedback">
                                        <div>
                                            <svg class="eksico"><use xlink:href="#eksico-drop"></use></svg>
                                            <span title="arşivlendiği andaki favori sayısı">${element.favoriteCount}</span>
                                        </div>
                                        <div class="archive-feedback-author">
                                            <span>${entryDateStr}</span>
                                            <a href="/biri/${element.author.replace(" ", "+")}" target="_blank">${element.author}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                }

                $("body").append(`
                    <div class="archives-modal" data-id="${id}">
                        <div class="archives-modal-title">
                            <h1>#${id} numaralı entry arşivi</h1>
                            <a href="javascript:;" class="modal-close">x</a>
                        </div>
                        <div id="entry-item-list">
                            <div id="accordion">
                                ${str}
                            </div>
                        </div>
                        <div class="archives-modal-footer">
                            <button type="button" class="modal-close cancel-button">kapat</button>
                        </div>
                    </div>
                    <div class="archives-modal-overlay"></div>
                `);

                $(".archives-modal #entry-item-list #title a").attr("target", "_blank").attr("title", "entry'nin yer aldığı başlık");
            }
        },
        error: function () {
            alert("Sunucuya bağlanırken bir hata oluştu!");
        }
    });
});

$(document).on("click", ".archives-modal .modal-close", function () {
    $(this).closest(".archives-modal").next(".archives-modal-overlay").remove();
    $(this).closest(".archives-modal").remove();
});

$(document).on("click", ".compare-btn", function () {
    var title = $(this).closest(".archives-modal").find(".archives-modal-title h1").text();

    var elem = $(this).closest(".card");
    var prevElem = $(this).closest(".card").next(".card");

    var text = $(elem).find(".archive-content").html().trim();
    var prevText = $(prevElem).find(".archive-content").html().trim();

    var date = $(elem).find(".card-title strong").text().trim();
    var prevDate = $(prevElem).find(".card-title strong").text().trim();

    $("body").append(`
                    <div class="archives-modal">
                        <div class="archives-modal-title">
                            <h1>${title}</h1>
                            <div><span class="archives-modal-subtitle"><b style="color: red;">${prevDate}</b> &rarr; <b style="color: green;">${date}</b> arşivleri arasındaki farklılıklar</span></div>
                            <a href="javascript:;" class="modal-close">x</a>
                        </div>
                        <div id="comparison-result">
                            <div class="comparison-title">
                                ${$("#title").prop('outerHTML')}
                            </div>
                        </div>
                        <div class="archives-modal-footer">
                            <button type="button" class="modal-close cancel-button">geri dön</button>
                        </div>
                    </div>
                    <div class="archives-modal-overlay"></div>
                `);

    let span = null;

    const diff = Diff.diffChars(prevText, text),
        display = document.getElementById('comparison-result'),
        fragment = document.createDocumentFragment();

    var unchangedColor = $("body").hasClass("light-theme") ? "#333" : "#bdbdbd";

    diff.forEach((part) => {
    // green for additions, red for deletions
    // grey for common parts
    const color = part.added ? 'green' :
        part.removed ? 'red' : unchangedColor;
    span = document.createElement('b');
    span.style.color = color;
    span.innerHTML = part.value;
    fragment.appendChild(span);
    });

    display.appendChild(fragment);
});